package lab11;

import java.awt.EventQueue;
import java.io.File;
import java.io.FilenameFilter;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.event.ListSelectionListener;

import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class App {

	private String text = "<html><head></head><body><p><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>"
			+ "<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>"
			+ "<p>Excepteur sint:"
			+ "<ul>"
			+ "	<li>occaecat cupidatat non proident</li>"
			+ "	<li>sunt in culpa qui officia deserunt</li>"
			+ "	<li>mollit anim id est laborum.</li>"
			+ "</ul></p></body></html>";
	
	private JFrame frmLab;
	private DefaultListModel<String> model = new DefaultListModel<>();
	private JList<String> list;
	
	private ScriptEngine engine;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frmLab.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		engine = new ScriptEngineManager().getEngineByName("nashorn");
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLab = new JFrame();
		frmLab.setTitle("Lab11");
		frmLab.setBounds(100, 100, 548, 372);
		frmLab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLab.getContentPane().setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(31, 38, 205, 273);
		frmLab.getContentPane().add(scrollPane_1);
		
		JTextPane textPane = new JTextPane();
		scrollPane_1.setViewportView(textPane);
		textPane.setContentType("text/html");
		textPane.setText(text);
		
		JButton btnRender = new JButton("Renderuj");
		btnRender.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					engine.eval("load('classpath:scripts/" + list.getSelectedValue() + "')");
					Invocable invocable = (Invocable) engine;
					Object funcResult = invocable.invokeFunction("process", text);
					textPane.setText((String)funcResult);
				} catch (ScriptException | NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnRender.setEnabled(false);
		btnRender.setBounds(300, 163, 175, 23);
		frmLab.getContentPane().add(btnRender);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(298, 38, 177, 102);
		frmLab.getContentPane().add(scrollPane);
		
		list = new JList<>();
		list.setModel(model);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (list.getSelectedIndex() == -1) {
					btnRender.setEnabled(false);
				}
				else {
					btnRender.setEnabled(true);
				}
			}
		});
		scrollPane.setViewportView(list);
		
		//UPDATE SCRIPTS LIST
		File f = new File("D:\\Users\\Karol\\Documents\\Uczelnia\\Semestr VI\\JAVA\\lab\\lab11\\bin\\scripts");
		File[] files = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith("js");
			}
		});
		for(File file : files) {
			model.addElement(file.getName());
		}
	}
}